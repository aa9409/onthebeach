require_relative 'job'
class JobList

  class CircularDependencyError < StandardError; end

  attr_accessor :input
  attr_accessor :jobs


  # pass a multiline string representing a list of jobs to be sorted. (see job.rb for format of a single line).
  # This method will raise CircularDependencyError if the list of jobs has a circular dependency
  def initialize(input)
    @input = input
    @jobs = input.split("\n").collect { |str| Job.new(str) }
    @jobs.each { |job| raise CircularDependencyError if has_circular_dependency?(job) }
  end


  def sort

    # a queue of jobs to process
    queue = jobs.dup

    # this array will hold the sorted list of jobs
    order = []


    while !queue.empty?
      queue.each do |job|

        # if this job has no dependencies or depends on a job which has already 
        # been processed, it no longer needs processing and can be moved to the sorted list
        if job.dependency.nil? || order.include?(job.dependency)
          order << job.id
          queue.delete(job)
        end

      end
    end # while

    order
  end


  private


  # recursive method which tries to satisfy all dependencies for a given job.
  # it uses 'path' to keep track of which jobs it has already checked, If it
  # encounters a job twice, it's a circular dependency
  def has_circular_dependency?(job, path=[])
    return false if job.dependency.nil?
    return true if path.include?(job.id)
    path << job.id
    target = jobs.find{ |t| t.id == job.dependency}
    has_circular_dependency?(target, path)
  end

end
