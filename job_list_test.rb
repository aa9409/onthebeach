require 'minitest/autorun'
require_relative 'job_list'


class JobListTest < MiniTest::Test


  def assert_order(result, first, last)
    x,y = result.index(first), result.index(last)
    assert !x.nil?, "#{first} not found in result"
    assert !y.nil?, "#{last} not found in result"
    assert x < y, "Expected '#{first}' to come before '#{last}' in result('#{result.join}')"
  end


  def sort(jobs)
    JobList.new(jobs).sort
  end


  def test_single_job_without_dependencies
    jobs = "a =>"
    result = sort(jobs)
    assert_equal "a", result.join
  end


  def test_multiple_jobs_without_dependencies
    jobs = <<-EOS
      a => 
      b =>
      c =>
    EOS
    result = sort(jobs)
    assert_equal 3, result.uniq.size
  end


  def test_multiple_jobs_with_single_dependency
    jobs = <<-EOS
      a =>
      b => c
      c =>
    EOS
    result = sort(jobs)
    assert_order(result, "c", "b")
  end


  def test_multiple_jobs_with_multiple_dependencies
    jobs = <<-EOS
      a =>
      b => c
      c => f
      d => a
      e => b
      f =>
    EOS
    result = sort(jobs)
    assert_order(result, 'f', 'c')
    assert_order(result, 'c', 'b')
    assert_order(result, 'b', 'e')
    assert_order(result, 'a', 'd')
    # also check that the same 6 characters from the input are present
    assert_equal result.uniq.length, 6
  end


  def test_jobs_with_self_dependency
    jobs = <<-EOS
      a =>
      b =>
      c => c
    EOS
    assert_raises(Job::SelfDependencyError) { sort(jobs) }
  end


  def test_jobs_with_circular_dependency
    jobs = <<-EOS
      a =>
      b => c
      c => f
      d => a
      e =>
      f => b
    EOS
    assert_raises(JobList::CircularDependencyError) { sort(jobs) }
  end


end
