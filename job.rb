class Job

  class SelfDependencyError < StandardError; end

  attr_accessor :id, :dependency

  # Pass a string in the form 'a => b' where a is the name of the job, and b
  # is the name of it's dependency. If there is no dependency, pass 'a =>'
  #
  # This method will raise Job::SelfDependencyError if a and b are equal
  #
  # NOTE: I would normally include much more error checking and validation but havn't
  # done so due to time constraints e.g. check that there is only one '=>' in the string
  def initialize(input)
    arr = input.strip.split(/ => ?/).each(&:strip!)
    @id = arr.first
    @dependency = arr.size == 2 ? arr.last : nil
    raise Job::SelfDependencyError if @id == @dependency
  end


  def ==(other)
    id == other.id &&
    dependency == other.dependency
  end


end
